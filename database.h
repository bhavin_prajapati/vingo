#ifndef DATABASE
#define DATABASE

#include <QtSql>

class Database : public QObject
{
    Q_OBJECT

public:
    Database(const QString &databaseName);

public slots:
    void clearDatabase();
    void store();
    void fetchTable();

private:
    QSqlDatabase *database;
};

#endif // DATABASE

