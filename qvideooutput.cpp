////////////////////////////////////////////////////////////////////////////////
//! @file   : qvideooutput.cpp
//! @date   : Feb 1 2014
//!
//! @brief  : Defines a QT style video output class
//!
//! The Basement Lab for Computer Vision and Personal Robotics
//! Copyright (C) 2013 - All Rights Reserved
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Includes
////////////////////////////////////////////////////////////////////////////////
#include "qvideooutput.h"
#include <stdio.h>
////////////////////////////////////////////////////////////////////////////////
// Macro redefinition since original does not compile in c++
///////////////////////////////////////////////////////////////////////////////
#undef av_err2str
#define av_err2str(errnum) \
        av_make_error_string(reinterpret_cast<char*>(_alloca(AV_ERROR_MAX_STRING_SIZE)),\
                             AV_ERROR_MAX_STRING_SIZE, errnum)
////////////////////////////////////////////////////////////////////////////////
//  QVideoOutput::QVideoOutput
//!
//! @brief Constructor
//!
//! @param[in] parent : A parent object.
//!
////////////////////////////////////////////////////////////////////////////////
QVideoOutput::QVideoOutput(QObject *parent)
: QObject(parent)
, swsContext(0x0)
, formatContext(0x0)
, outputFormat(0x0)
, videoStream(0x0)
, videoCodec(0x0)
, frame(0x0)
, swsFlags(SWS_BICUBIC)
, streamPixFmt(AV_PIX_FMT_YUV420P) // default pix_fmt
, streamFrameRate(25)              // 25 images/s
, width(640)
, height(480)
{
   // Init FFmpeg
   av_register_all();
}
////////////////////////////////////////////////////////////////////////////////
//  QVideoOutput::~QVideoOutput
//!
//! @brief Destructor
//!
////////////////////////////////////////////////////////////////////////////////
QVideoOutput::~QVideoOutput()
{
}
////////////////////////////////////////////////////////////////////////////////
//  QVideoOutput::setResolution
//!
//! @brief Sets resolution.
//!
//! @param[in] inWidth  : Specifies width. Must be a multiple of two
//! @param[in] inHeight : Specifies height. Must be a multiple of two
//!
////////////////////////////////////////////////////////////////////////////////
void QVideoOutput::setResolution(int inWidth, int inHeight)
{
   Q_ASSERT(inWidth%2  == 0);
   Q_ASSERT(inHeight%2 == 0);
   width  = inWidth;
   height = inHeight;
}
////////////////////////////////////////////////////////////////////////////////
//  QVideoOutput::addStream
//!
//! @brief Adds stream
//!
//! @param[in]  inFormatContext
//! @param[out] codec
//! @param[in]  codecId
//!
////////////////////////////////////////////////////////////////////////////////
AVStream *QVideoOutput::addStream(AVFormatContext * inFormatContext,
                                  AVCodec **codec,
                                  AVCodecID codecId)const
{
    AVCodecContext *c;
    AVStream *st;
    // find the encoder
    *codec = avcodec_find_encoder(codecId);
    if (!(*codec))
    {
        fprintf(stderr, "Could not find encoder for '%s'\n",
                avcodec_get_name(codecId));
        return 0x0;
    }
    st = avformat_new_stream(inFormatContext, *codec);
    if (!st)
    {
        fprintf(stderr, "Could not allocate stream\n");
        return 0x0;
    }
    st->id = inFormatContext->nb_streams-1;
    c = st->codec;
    switch ((*codec)->type)
    {
    case AVMEDIA_TYPE_AUDIO:
        st->id = 1;
        c->sample_fmt  = AV_SAMPLE_FMT_S16;
        c->bit_rate    = 64000;
        c->sample_rate = 44100;
        c->channels    = 2;
        break;
    case AVMEDIA_TYPE_VIDEO:
        avcodec_get_context_defaults3(c, *codec);
        c->codec_id = codecId;
        c->bit_rate = 400000;
        c->width    = width;
        c->height   = height;
        // timebase: This is the fundamental unit of time (in seconds) in terms
        // of which frame timestamps are represented. For fixed-fps content,
        // timebase should be 1/framerate and timestamp increments should be
        // identical to 1.
        //c->time_base.den = streamFrameRate;
        //c->time_base.num = 1;
        c->time_base.den = streamFrameRate;
        c->time_base.num = 1;
        c->gop_size      = 12; // emit one intra frame every twelve frames at most
        c->pix_fmt       = streamPixFmt;
        if (c->codec_id == AV_CODEC_ID_MPEG2VIDEO)
        {
            // just for testing, we also add B frames
            c->max_b_frames = 2;
        }
        if (c->codec_id == AV_CODEC_ID_MPEG1VIDEO)
        {
            // Needed to avoid using macroblocks in which some coeffs overflow.
            // This does not happen with normal video, it just happens here as
            // the motion of the chroma plane does not match the luma plane.
            c->mb_decision = 2;
        }
    break;
    default:
        break;
    }
    // Some formats want stream headers to be separate.
    if (inFormatContext->oformat->flags & AVFMT_GLOBALHEADER)
        c->flags |= CODEC_FLAG_GLOBAL_HEADER;
    return st;
}
////////////////////////////////////////////////////////////////////////////////
//  QVideoOutput::openVideo
//!
//! @brief Opens video
//!
//! @param[in]  codec
//! @param[in]  stream
//!
////////////////////////////////////////////////////////////////////////////////
bool QVideoOutput::openVideo(AVCodec *codec, AVStream *stream)
{
    int ret;
    AVCodecContext *c = stream->codec;
    // open the codec
    ret = avcodec_open2(c, codec, NULL);
    if (ret < 0)
    {
       fprintf(stderr, "Could not open video codec: %s\n", av_err2str(ret));
       return false;
    }
    // allocate and init a re-usable frame
    frame = avcodec_alloc_frame();
    if (!frame)
    {
       fprintf(stderr, "Could not allocate video frame\n");
       return false;
    }
    // Allocate the encoded raw picture.
    ret = avpicture_alloc(&dstPicture, c->pix_fmt, c->width, c->height);
    if (ret < 0)
    {
        fprintf(stderr, "Could not allocate picture: %s\n", av_err2str(ret));
        return false;
    }
    // copy data and linesize picture pointers to frame
    *((AVPicture *)frame) = dstPicture;
    return true;
}
////////////////////////////////////////////////////////////////////////////////
//  QVideoOutput::writeVideoFrame
//!
//! @brief Writes video frame
//!
//! @param[in]  src
//! @param[in]  srcWidth
//! @param[in]  srcHeight
//! @param[in]  inFormatContext
//! @param[in]  stream
//!
////////////////////////////////////////////////////////////////////////////////
bool QVideoOutput::writeVideoFrame(const AVPicture &src,
                                   int srcWidth,
                                   int srcHeight,
                                   AVFormatContext * inFormatContext,
                                   AVStream *stream)
{
    int ret;
    AVCodecContext *c = stream->codec;
    if (c->pix_fmt != AV_PIX_FMT_RGBA)
    {
       // as we only use RGBA picture, we must convert it
       // to the codec pixel format if needed
      if (!swsContext)
      {
           swsContext = sws_getContext(srcWidth,
                                       srcHeight,
                                       AV_PIX_FMT_BGRA,
                                       c->width,
                                       c->height,
                                       c->pix_fmt,
                                       swsFlags,
                                       NULL,
                                       NULL,
                                       NULL);
           if (!swsContext)
           {
               fprintf(stderr, "Could not initialize the conversion context\n");
               return false;
           }
       }
       sws_scale(swsContext,
                 (const uint8_t * const *)src.data,
                 src.linesize,
                 0,
                 c->height,
                 dstPicture.data,
                 dstPicture.linesize);
    }
    if (inFormatContext->oformat->flags & AVFMT_RAWPICTURE)
    {
        // Raw video case - directly store the picture in the packet
        AVPacket pkt;
        av_init_packet(&pkt);
        pkt.flags        |= AV_PKT_FLAG_KEY;
        pkt.stream_index  = stream->index;
        pkt.data          = dstPicture.data[0];
        pkt.size          = sizeof(AVPicture);
        ret = av_interleaved_write_frame(inFormatContext, &pkt);
    }
    else
    {
        // encode the image
        AVPacket pkt;
        int gotOutput;
        av_init_packet(&pkt);
        pkt.data = NULL;    // packet data will be allocated by the encoder
        pkt.size = 0;
        ret = avcodec_encode_video2(c, &pkt, frame, &gotOutput);
        if (ret < 0)
        {
            fprintf(stderr, "Error encoding video frame: %s\n",
                    av_err2str(ret));
            return false;
        }
        // If size is zero, it means the image was buffered.
        if (gotOutput)
        {
            if (c->coded_frame->key_frame)
                pkt.flags |= AV_PKT_FLAG_KEY;
            pkt.stream_index = stream->index;
            // Write the compressed frame to the media file.
            ret = av_interleaved_write_frame(inFormatContext, &pkt);
        }
        else
        {
            ret = 0;
        }
    }
    if (ret != 0)
    {
        fprintf(stderr, "Error while writing video frame: %s\n",
                av_err2str(ret));
        return false;
    }
    frameCount++;
    return true;
}
////////////////////////////////////////////////////////////////////////////////
//  QVideoOutput::openMediaFile
//!
//! @brief Opens media file
//!
//! @param[in]  width    :
//! @param[in]  height   :
//! @param[in]  filename :
//!
////////////////////////////////////////////////////////////////////////////////
bool QVideoOutput::openMediaFile(int width, int height, const QString & filename)
{
   // allocate the output media context
   avformat_alloc_output_context2(&formatContext, NULL, NULL, filename.toLocal8Bit().data());
   if (!formatContext)
   {
       printf("Could not deduce output format from file extension: using MPEG.\n");
       avformat_alloc_output_context2(&formatContext, NULL, "mpeg", filename.toLocal8Bit().data());
   }
   if (!formatContext)
   {
       return false;
   }
   outputFormat = formatContext->oformat;
   // Add the video streams using the default format codecs
   // and initialize the codecs.
   videoStream = NULL;
   if (outputFormat->video_codec != AV_CODEC_ID_NONE)
   {
       videoStream = addStream(formatContext, &videoCodec, outputFormat->video_codec);
   }
   // Now that all the parameters are set, we can open the audio and
   // video codecs and allocate the necessary encode buffers.
   if (videoStream)
       openVideo(videoCodec, videoStream);
   av_dump_format(formatContext, 0, filename.toLocal8Bit().data(), 1);
   int ret = 0;
   // open the output file, if needed
   if (!(outputFormat->flags & AVFMT_NOFILE))
   {
       ret = avio_open(&formatContext->pb, filename.toLocal8Bit().data(), AVIO_FLAG_WRITE);
       if (ret < 0)
       {
           fprintf(stderr, "Could not open '%s': %s\n", filename, av_err2str(ret));
           return false;
       }
   }
   // Write the stream header, if any.
   ret = avformat_write_header(formatContext, NULL);
   if (ret < 0)
   {
       fprintf(stderr, "Error occurred when opening output file: %s\n", av_err2str(ret));
       return false;
   }
   if (frame)
       frame->pts = 0;
   return avpicture_alloc(&srcPicture, AV_PIX_FMT_RGBA, width, height) >= 0;
}
////////////////////////////////////////////////////////////////////////////////
//  QVideoOutput::newFrame
//!
//! @brief Adds new frame to ouput stream
//!
//! @param[in]  image :
//!
////////////////////////////////////////////////////////////////////////////////
bool QVideoOutput::newFrame(const QImage & image)
{
   const int width  = image.width();
   const int height = image.height();
   // write video frames
   for (int y = 0; y < height; y++)
   {
      const uint8_t * scanline = image.scanLine(y);
      for (int x = 0; x < width*4; x++)
      {
         srcPicture.data[0][y * srcPicture.linesize[0] + x] = scanline[x];
      }
   }
   writeVideoFrame(srcPicture, width, height, formatContext, videoStream);
   frame->pts += av_rescale_q(1,
                              videoStream->codec->time_base,
                              videoStream->time_base);
   return true;
}
////////////////////////////////////////////////////////////////////////////////
//  QVideoOutput::closeMediaFile
//!
//! @brief Closes media file
//!
////////////////////////////////////////////////////////////////////////////////
bool QVideoOutput::closeMediaFile()
{
   av_free(srcPicture.data[0]);
   // Write the trailer, if any. The trailer must be written before you
   // close the CodecContexts open when you wrote the header; otherwise
   // av_write_trailer() may try to use memory that was freed on
   // av_codec_close().
   av_write_trailer(formatContext);
   // Close each codec.
   if (videoStream)
       closeVideo(videoStream);
   if (swsContext)
   {
      sws_freeContext(swsContext);
      swsContext = 0x0;
   }
   // Free the streams.
   for (unsigned int i = 0; i < formatContext->nb_streams; i++)
   {
       av_freep(&formatContext->streams[i]->codec);
       av_freep(&formatContext->streams[i]);
   }
   if (!(outputFormat->flags & AVFMT_NOFILE))
   {
      // Close the output file.
      avio_close(formatContext->pb);
   }
   // free the stream
   av_free(formatContext);
   return true;
}
////////////////////////////////////////////////////////////////////////////////
//  QVideoOutput::closeVideo
//!
//! @brief Closes video
//!
//! @param[in]  stream :
//!
////////////////////////////////////////////////////////////////////////////////
void QVideoOutput::closeVideo(AVStream *stream)
{
    avcodec_close(stream->codec);
    av_free(dstPicture.data[0]);
    av_free(frame);
}
