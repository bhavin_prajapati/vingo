////////////////////////////////////////////////////////////////////////////////
//! @file   : qvideooutput.h
//! @date   : Feb 1 2014
//!
//! @brief  : Declares a QT style video output class
//!
//! The Basement Lab for Computer Vision and Personal Robotics
//! Copyright (C) 2013 - All Rights Reserved
////////////////////////////////////////////////////////////////////////////////
#ifndef QVIDEOOUTPUT_H
#define QVIDEOOUTPUT_H
////////////////////////////////////////////////////////////////////////////////
// Includes
////////////////////////////////////////////////////////////////////////////////
#include "QObject"
#include "QImage"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

extern "C" {
#include <libavutil/mathematics.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
}

////////////////////////////////////////////////////////////////////////////////
//! @class : QVideoOutput
//!
//! @brief : Implements a Qt style wrapper class for some functions
//!          from the FFmpeg library
//!
////////////////////////////////////////////////////////////////////////////////
class QVideoOutput : public QObject
{
    Q_OBJECT

public:
   QVideoOutput(QObject * parent=0);
   virtual ~QVideoOutput();
   bool openMediaFile(int width,
                      int height,
                      const QString & filename);
   bool closeMediaFile();
   void setResolution(int width, int height);
public slots:
   bool newFrame(const QImage & image);
protected:
   // Protected methods ////////////////////////////////////////////////////////
   AVStream * addStream(AVFormatContext *inFormatContext,
                        AVCodec **codec,
                        AVCodecID codecId)const;
   bool openVideo(AVCodec *codec, AVStream *st);
   bool writeVideoFrame(const AVPicture &src,
                        int srcWidth,
                        int srcHeight,
                        AVFormatContext *inFormatContext,
                        AVStream *st);
   void closeVideo(AVStream *st);
   // Protected members ////////////////////////////////////////////////////////
   AVFormatContext * formatContext;
   AVOutputFormat  * outputFormat;
   AVStream        * videoStream;
   AVCodec         * videoCodec;
   SwsContext      * swsContext;
   AVFrame         * frame;
   AVPicture srcPicture;
   AVPicture dstPicture;
   AVPixelFormat streamPixFmt;
   int swsFlags;
   int streamFrameRate;
   int width;
   int height;
   int frameCount;
};
#endif // QVIDEOOUTPUT_H
