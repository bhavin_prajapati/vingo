#include "database.h"
#include <QtSql>

Database::Database(const QString &databaseName){
    database = new QSqlDatabase();
    //set database driver to QSQLITE
    *database = QSqlDatabase::addDatabase("QSQLITE");
    database->setDatabaseName("./" + databaseName);
    QSqlTableModel *all_model = new QSqlTableModel(this, *database);
    all_model->setTable("Contacts");
    all_model->select();
}

void Database::clearDatabase(){

}

void Database::fetchTable(){

}

void Database::store(){

}
