TEMPLATE = app

QT += core \
      network \
      xml \
      multimedia \
      multimediawidgets \
      widgets \
      sql

FFMPEG_DIR = $$PWD/ffmpeg-20150131-git-f5722ba-win64-shared/bin
MY_LIB_FILES += $${FFMPEG_DIR}/avcodec-56.dll
MY_LIB_FILES += $${FFMPEG_DIR}/avdevice-56.dll
MY_LIB_FILES += $${FFMPEG_DIR}/avfilter-5.dll
MY_LIB_FILES += $${FFMPEG_DIR}/avformat-56.dll
MY_LIB_FILES += $${FFMPEG_DIR}/avutil-54.dll
MY_LIB_FILES += $${FFMPEG_DIR}/postproc-53.dll
MY_LIB_FILES += $${FFMPEG_DIR}/swresample-1.dll
MY_LIB_FILES += $${FFMPEG_DIR}/swscale-3.dll

INCLUDEPATH += $$PWD/ffmpeg-20150131-git-f5722ba-win64-dev/include
# linker settings for ffmpeg ###################################################
win32:LIBS += $$PWD/ffmpeg-20150131-git-f5722ba-win64-dev/lib/avcodec.lib
win32:LIBS += $$PWD/ffmpeg-20150131-git-f5722ba-win64-dev/lib/avdevice.lib
win32:LIBS += $$PWD/ffmpeg-20150131-git-f5722ba-win64-dev/lib/avfilter.lib
win32:LIBS += $$PWD/ffmpeg-20150131-git-f5722ba-win64-dev/lib/avformat.lib
win32:LIBS += $$PWD/ffmpeg-20150131-git-f5722ba-win64-dev/lib/avutil.lib
win32:LIBS += $$PWD/ffmpeg-20150131-git-f5722ba-win64-dev/lib/postproc.lib
win32:LIBS += $$PWD/ffmpeg-20150131-git-f5722ba-win64-dev/lib/swresample.lib
win32:LIBS += $$PWD/ffmpeg-20150131-git-f5722ba-win64-dev/lib/swscale.lib

QMAKE_LFLAGS += /OPT:NOREF

HEADERS += colorswatch.h mainwindow.h toolbar.h \
    histogramwidget.h \
    player.h \
    playercontrols.h \
    playlistmodel.h \
    videowidget.h \
    qvideooutput.h \
    database.h \
    qplayer.h


SOURCES += colorswatch.cpp mainwindow.cpp toolbar.cpp main.cpp \
    histogramwidget.cpp \
    player.cpp \
    playercontrols.cpp \
    playlistmodel.cpp \
    videowidget.cpp \
    qvideooutput.cpp \
    database.cpp \
    qplayer.cpp

build_all:!build_pass {
    CONFIG -= build_all
    CONFIG += release
}

CONFIG(debug, debug|release) {
    DESTDIR = debug
} else {
    DESTDIR = release
}

RESOURCES += mainwindow.qrc

# install
ffmpeg.files = $$PWD/ffmpeg-20150131-git-f5722ba-win64-shared/bin/*
ffmpeg.path = $$OUT_PWD/$$DESTDIR
target.path = $$[QT_INSTALL_EXAMPLES]/widgets/mainwindows/mainwindow

INSTALLS += target ffmpeg
